#Run instructions (localhost only)

## Backend
- Install PHP 8.x
- Install Symfony ()
- Install Composer ()
- Run `composer install` in `./backend`
- Run `symfony server:ca:install` in `./backend`
- Run `symfony serve` in `./backend`

## Frontend
- Install NodeJS
- Run `npm install` in `./frontend`
- Run `npm start` in `./frontend`