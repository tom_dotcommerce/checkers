<?php

namespace App\Controller;

use App\Game\Board;
use PHPUnit\Util\Json;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\Exception\JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{
    protected function returnError(string $error) : JsonResponse {
        return new JsonResponse(compact('error'));
    }

    #[Route('/api/getNewGame', name: 'newGame')]
    public function newGame(): Response
    {
        $board = new Board();
        return new Response(content:$board->toJson(),headers: ["Content-type" => "application/json"]);
    }

    #[Route('/api/makeMove', name: 'makeMove')]
    public function makeMove(Request $request): Response
    {
        $data = $request->request->get('data');

        $board = Board::fromJson($data);

        if($board->isWhitePlayerTurn()) {
            $move = $request->request->get('move');
            $move = json_decode($move, true);

            if ($move === null) return $this->returnError("Invalid move JSON");
            if (count($move) !== 2) return $this->returnError("Invalid move count");

            try {
                $board->move($move[0], $move[1]);
            } catch (\Exception $e) {
                return $this->returnError($e->getMessage());
            }
        }

        if(!$board->isWhitePlayerTurn()) {
            $validMoves = $board->getValidMoves();
            if (count($validMoves) > 0) {
                shuffle($validMoves);
                $move = $validMoves[0];
                $board->move($move[0], $move[1]);
            }
        }

        return new Response(content:$board->toJson(),headers: ["Content-type" => "application/json"]);
    }
}
