<?php
declare(strict_types=1);

namespace App\Game;

use \Exception;

class Board
{
    public function __construct(
        protected array $boardField = [
            " x x x x x",
            "x x x x x ",
            " x x x x x",
            "x x x x x ",
            " _ _ _ _ _",
            "_ _ _ _ _ ",
            " o o o o o",
            "o o o o o ",
            " o o o o o",
            "o o o o o ",
        ],
        protected bool  $whitePlayerTurn = true,
        protected int   $size = 10,
        protected ?array $lastMove = null,
        protected ?array $lastStart = null,
    )
    {
    }

    /**
     * @param string $json
     * @return Board
     * @throws Exception
     */
    public static function fromJson(string $json): Board
    {
        $data = json_decode($json, true);
        if ($data === null) {
            throw new Exception("Invalid JSON");
        }

        if (count($data['boardField']) !== $data['size']) {
            throw new Exception("Invalid number of board rows");
        }

        foreach ($data['boardField'] as $y => $row) {
            if (strlen($row) !== $data['size']) {
                throw new Exception("Invalid number of fields in row {$y}");
            }

            for ($x = 0; $x < $data['size']; $x++) {
                if (($x + $y) % 2 == 0 && $row[$x] != ' ') { // Even
                    throw new Exception("Invalid field at {$x},{$y}");
                }

                if (($x + $y) % 2 == 1 && !in_array($row[$x], ["x", "o", "_", "!", "O", "X"])) { // Odd
                    throw new Exception("Invalid field at {$x},{$y}");
                }
            }
        }

        if(
            $data['lastMove'] !== null &&
            !($data['lastMove'][0] < $data['size'] && $data['lastMove'][0] >= 0
                && $data['lastMove'][1] < $data['size'] && $data['lastMove'][1] >= 0)) {
            throw new Exception("Invalid last move");
        }

        return new Board($data['boardField'], $data['whitePlayerTurn'], $data['size'], $data['lastMove']);
    }

    public function toJson(): string
    {
        $data = [
            'boardField' => $this->boardField,
            'whitePlayerTurn' => $this->whitePlayerTurn,
            'size' => $this->size,
            'lastMove' => $this->lastMove,
            'lastStart' => $this->lastStart,
            'validMoves' => $this->getValidMoves()
        ];

        return json_encode($data);
    }

    protected function generateValidMovesForPosition(
        int    $x,
        int    $y,
        string $lookFor,
        array  $directions,
        array  $validMoves = [],
        bool   &$hasCaptures = false,
        bool   $captureOnly = false,
        int    $depth = 1,
        int   &$currentMaxLevel = 0,
        array  $allDirections = [],
    ): array
    {
        $stepSize = 2;
        if (strtolower($this->boardField[$y][$x]) != $lookFor) return $validMoves;
        if (strtoupper($this->boardField[$y][$x]) === $this->boardField[$y][$x]) {
            $directions = $allDirections;
            $stepSize = 10;
        }
        foreach ($directions as $direction) {
            if (
                !$this->isValidPosition([
                    $x + $direction[0],
                    $y + $direction[1]
                ])
            ) continue;
            for($i = 1; $i < $stepSize; $i++) {
                if (
                    !$this->isValidPosition([
                        $x + $direction[0] * $i,
                        $y + $direction[1] * $i
                    ])
                ) continue 2;
                switch (strtolower($this->boardField[$y + $direction[1] * $i][$x + $direction[0] * $i])) {
                    case "o":
                    case "x":
                        if (strtolower($this->boardField[$y + $direction[1] * $i][$x + $direction[0] * $i]) == $lookFor) continue 3;

                        if (
                            !$this->isValidPosition([
                                $x + $direction[0] * ($i + 1),
                                $y + $direction[1] * ($i + 1)
                            ])
                        ) continue 2;

                        if ($this->boardField[$y + $direction[1] * ($i + 1)][$x + $direction[0] * ($i + 1)] == "_") {
                            if ($hasCaptures === false) {
                                $validMoves = [];
                            }
                            $hasCaptures = true;

                            for($k = $i; $k < $stepSize; $k++) {
                                if (
                                    !$this->isValidPosition([
                                        $x + $direction[0] * ($k + 1),
                                        $y + $direction[1] * ($k + 1)
                                    ])
                                ) continue 3;

                                if(
                                    $this->boardField[$y + $direction[1] * ($k + 1)][$x + $direction[0] * ($k + 1)] != '_'
                                ) continue 3;

                                $myToken = $this->boardField[$y][$x];
                                $oldValue = $this->boardField[$y + $direction[1] * $i][$x + $direction[0] * $i];
                                $this->boardField[$y + $direction[1] * ($k + 1)][$x + $direction[0] * ($k + 1)] = $myToken;
                                $this->boardField[$y + $direction[1] * $i][$x + $direction[0] * $i] = "!";
                                $this->boardField[$y][$x] = "_";
                                $tempCurrentMaxLevel = $currentMaxLevel;

                                $multiJumpMoves = $this->generateValidMovesForPosition(
                                    x: $x + $direction[0] * ($k + 1),
                                    y: $y + $direction[1] * ($k + 1),
                                    lookFor: $lookFor,
                                    directions: $allDirections,
                                    validMoves: [],
                                    hasCaptures: $hasCaptures,
                                    captureOnly: true,
                                    depth: $depth + 1,
                                    currentMaxLevel: $tempCurrentMaxLevel,
                                    allDirections: $allDirections,
                                );

                                $this->boardField[$y + $direction[1] * ($k + 1)][$x + $direction[0] * ($k + 1)] = "_";
                                $this->boardField[$y + $direction[1] * $i][$x + $direction[0] * $i] = $oldValue;
                                $this->boardField[$y][$x] = $myToken;

                                $maxLevel = $depth;
                                foreach ($multiJumpMoves as $multiJumpMove) {
                                    $maxLevel = max($multiJumpMove[2], $maxLevel);
                                }

                                if ($currentMaxLevel < $maxLevel) {
                                    $currentMaxLevel = $maxLevel;
                                    $validMoves = [];
                                }

                                if ($currentMaxLevel <= $maxLevel) {
                                    $validMoves[] = [[$x, $y], [$x + $direction[0] * ($k + 1), $y + $direction[1] * ($k + 1)], $maxLevel];
                                }
                            }
                        }

                        break;
                    case "_":
                        if ($hasCaptures || $captureOnly) continue 2;
                        $validMoves[] = [[$x, $y], [$x + $direction[0] * $i, $y + $direction[1] * $i], $depth];
                        break;
                    default:
                        continue 2;
                }
            }
        }
        return $validMoves;
    }

    protected
    function generateValidMoves(
        string $lookFor,
        array  $directions,
        array  $validMoves = [],
        ?bool  &$hasCaptures = false,
        bool   $captureOnly = false,
        array  $allDirections = [],
        ?int  &$currentMaxLevel = 0,
    ): array
    {

        if ($currentMaxLevel === null) $currentMaxLevel = 0;
        if ($hasCaptures === null) $hasCaptures = false;

        for ($y = 0; $y < $this->size; $y++) {
            for ($x = 0; $x < $this->size; $x++) {
                if($this->boardField[$y][$x] === strtoupper($this->boardField[$y][$x]) && $captureOnly) continue;

                $validMoves = $this->generateValidMovesForPosition(
                    x: $x,
                    y: $y,
                    lookFor: $lookFor,
                    directions: $directions,
                    validMoves: $validMoves,
                    hasCaptures: $hasCaptures,
                    captureOnly: $captureOnly,
                    currentMaxLevel: $currentMaxLevel,
                    allDirections: $allDirections,
                );
            }
        }
        return $validMoves;
    }

    public
    function getValidMoves(): array
    {
        if ($this->whitePlayerTurn) {
            $lookFor = "o";
            $directions = [
                [-1, -1],
                [1, -1],
            ];
            $captureDirections = [
                [-1, 1],
                [1, 1],
            ];
        } else {
            $lookFor = "x";
            $directions = [
                [-1, 1],
                [1, 1],
            ];
            $captureDirections = [
                [-1, -1],
                [1, -1],
            ];
        }

        if(
            $this->lastMove !== null &&
            $this->boardField[$this->lastMove[1]][$this->lastMove[0]] === $lookFor
        ) {
            $hasCaptures = true;
            return $this->generateValidMovesForPosition(
                x: $this->lastMove[0],
                y: $this->lastMove[1],
                lookFor: $lookFor,
                directions: array_merge($directions,$captureDirections),
                validMoves: [],
                hasCaptures: $hasCaptures,
                captureOnly: true,
                allDirections: array_merge($directions,$captureDirections)
            );
        }

        $array_merge = array_merge($directions, $captureDirections);
        $validMoves = $this->generateValidMoves(
            lookFor: $lookFor,
            directions: $directions,
            hasCaptures: $hasCaptures,
            allDirections: $array_merge,
            currentMaxLevel: $currentMaxLevel,
        );

        return $this->generateValidMoves(
            lookFor: $lookFor,
            directions: $captureDirections,
            validMoves: $validMoves,
            hasCaptures: $hasCaptures,
            captureOnly: true,
            allDirections: $array_merge,
            currentMaxLevel: $currentMaxLevel,
        );
    }

    /**
     * @return string[]
     */
    public
    function getBoardField(): array
    {
        return $this->boardField;
    }

    public
    function isValidPosition(array $pos): bool
    {
        return $pos[0] < $this->size && $pos[0] >= 0 && $pos[1] < $this->size && $pos[1] >= 0;
    }

    public
    function move(array $from, array $to): void
    {
        if (count($from) !== 2 || count($to) !== 2) throw new \Exception("Invalid move (coord)");
        if (!$this->isValidPosition($from) || !$this->isValidPosition($to)) throw new \Exception("Invalid move (OOB)");

        $myToken = $this->whitePlayerTurn ? "o" : "x";
        if (strtolower($this->boardField[$from[1]][$from[0]]) != $myToken) throw new \Exception("Invalid move (owner)");
        if ($this->boardField[$to[1]][$to[0]] != "_") throw new \Exception("Invalid move (target)");

        $validMoves = $this->getValidMoves();
        $userMove = null;

        foreach($validMoves as $validMove) {
            if($validMove[0] == $from && $validMove[1] == $to) {
                $userMove = $validMove;
                break;
            }
        }

        if ($userMove === null) throw new \Exception("Invalid move (path)");

        $stepSize = abs($from[0] - $to[0]);
        $direction = [
            ($to[0] - $from[0]) / $stepSize,
            ($to[1] - $from[1]) / $stepSize,
        ];

        $oldValue = null;

        $currentPosition = $from;
        $startPosition = true;
        do {
            if(!$startPosition && $this->boardField[$currentPosition[1]][$currentPosition[0]] != "_") {
                $this->boardField[$currentPosition[1]][$currentPosition[0]] = "!";
            } else {
                if($startPosition) {
                    $oldValue = $this->boardField[$currentPosition[1]][$currentPosition[0]];
                }
                $this->boardField[$currentPosition[1]][$currentPosition[0]] = "_";
            }

            $currentPosition = [
                $currentPosition[0] + $direction[0],
                $currentPosition[1] + $direction[1],
            ];
            $startPosition = false;
        } while ($currentPosition !== $to);

        $this->boardField[$to[1]][$to[0]] = $oldValue;

        $this->lastMove = $to;
        $this->lastStart = $from;

        if($userMove[2] == 1) {
            if(
                ($myToken === "o" && $this->lastMove[1] === 0) ||
                $myToken === "x" && $this->lastMove[1] === $this->size - 1
            ) {
                $this->boardField[$to[1]][$to[0]] = strtoupper($myToken);
            }

            $this->whitePlayerTurn = !$this->whitePlayerTurn;
            foreach($this->boardField as $key => $row) {
                $this->boardField[$key] = str_replace("!","_",$this->boardField[$key]);
            }
        }
    }

    /**
     * @return bool
     */
    public
    function isWhitePlayerTurn(): bool
    {
        return $this->whitePlayerTurn;
    }


}
