<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewGameTest extends WebTestCase
{
    public function testNewGameApi(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/getNewGame');

        $data = json_decode($client->getResponse()->getContent(),true);
        $this->assertResponseIsSuccessful();
        $this->assertEquals($data['boardField'], [
            " x x x x x",
            "x x x x x ",
            " x x x x x",
            "x x x x x ",
            " _ _ _ _ _",
            "_ _ _ _ _ ",
            " o o o o o",
            "o o o o o ",
            " o o o o o",
            "o o o o o ",
        ]);
        $this->assertEquals($data['whitePlayerTurn'], true);
        $this->assertEquals($data['size'], 10);
    }
}
