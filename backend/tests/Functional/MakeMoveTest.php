<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MakeMoveTest extends WebTestCase
{
    public function testMoveApi(): void
    {
        $client = static::createClient();

        $client->xmlHttpRequest('POST', '/api/makeMove',[
                "data" => '{"boardField":[" x x x x x","x x x x x "," x x x x x","x x x x x "," _ _ _ _ _","_ _ _ _ _ "," o o o o o","o o o o o "," o o o o o","o o o o o "],"whitePlayerTurn":true,"size":10,"lastMove":null}',
                "move" => "[[1,6],[2,5]]",
        ]);

        $this->assertResponseIsSuccessful();

        $response = $client->getResponse()->getContent();
        $data = json_decode($response,true);

        $this->assertNotEquals("_ _ _ _ _ ",$data['boardField'][5]);
        $this->assertNotEquals("_ _ _ _ _ ",$data['boardField'][5]);
        $this->assertNotEquals(" o o o o o",$data['boardField'][6]);

        $this->assertNotEquals(" _ _ _ _ _",$data['boardField'][4]);
        $this->assertNotEquals("x x x x x ",$data['boardField'][3]);
    }
}
