<?php

namespace App\Tests\Unit;

use App\Game\Board;
use PHPUnit\Framework\TestCase;

class BoardTest extends TestCase
{
    public function testJson(): void
    {
        $json = '{"boardField":[" x x x x x","x x x x x "," x x x x x","x x x x x "," _ _ _ _ _","_ _ _ _ _ "," o o o o o","o o o o o "," o o o o o","o o o o o "],"whitePlayerTurn":true,"size":10,"lastMove":null}';
        $board = Board::fromJson($json);
        $data = $board->toJson();
        $this->assertEquals($json,$data);
        $this->assertIsArray(json_decode($data,true));

        $json = '{"boardField":[" x x x x x,"x x x x x "," x x x x x","x x x x x "," _ _ _ _ _","_ _ _ _ _ "," o o o o o","o o o o o "," o o o o o","o o o o o ",],"whitePlayerTurn":true,"size":10}';
        $this->expectExceptionMessage("Invalid JSON");
        Board::fromJson($json);
    }

    public function testInvalidBoardRows() : void {
        $this->expectExceptionMessage("Invalid number of board rows");
        $json = '{"boardField":[" x x x x x","x x x x x "," x x x x x","_ _ _ _ _ "," _ _ _ _ _","o o o o o "," o o o o o","o o o o o "],"whitePlayerTurn":true,"size":10}';
        Board::fromJson($json);
    }

    public function testInvalidBoardColumns() : void {
        $this->expectExceptionMessage("Invalid number of fields in row 0");
        $json = '{"boardField":[" x x x x ","x x x x x "," x x x x x","x x x x x "," _ _ _ _ _","_ _ _ _ _ "," o o o o o","o o o o o "," o o o o o","o o o o o "],"whitePlayerTurn":true,"size":10}';
        Board::fromJson($json);
    }

    public function testInvalidBoardFields() : void {
        $this->expectExceptionMessage("Invalid field at 0,2");
        $json = '{"boardField":[" x x x x x","x x x x x ","x x x x x "," x x x x x"," _ _ _ _ _","_ _ _ _ _ "," o o o o o","o o o o o "," o o o o o","o o o o o "],"whitePlayerTurn":true,"size":10}';
        Board::fromJson($json);
    }

    public function testValidMoves() : void {
        $board = new Board();
        $moves = $board->getValidMoves();
        $this->assertCount(9,$moves);

        $board = new Board(boardField:[
            " x x x x x",
            "x x x x x ",
            " x x x x x",
            "x x _ x x ",
            " _ x _ _ _",
            "_ o _ _ _ ",
            " _ o o o o",
            "o o o o o ",
            " o o o o o",
            "o o o o o ",
        ]);
        $moves = $board->getValidMoves();
        $this->assertCount(1,$moves);

        $board = new Board(boardField:[
           //0123456789
            " x x x x x", // 0
            "x x x x x ", // 1
            " x x x x x", // 2
            "x x _ _ _ ", // 3
            " _ x x x _", // 4
            "_ o o o _ ", // 5
            " _ _ _ o o", // 6
            "o o o o o ", // 7
            " o o o o o", // 8
            "o o o o o ", // 9
        ]);
        $moves = $board->getValidMoves();
        $this->assertCount(1,$moves);


        $board = new Board(boardField:[
           //0123456789
            " x x x x x", // 0
            "x x x x x ", // 1
            " x x x x x", // 2
            "x x _ x x ", // 3
            " _ o _ _ _", // 4
            "_ x _ _ _ ", // 5
            " _ o o o o", // 6
            "o o o o o ", // 7
            " o o o o o", // 8
            "o o o o o ", // 9
        ]);
        $moves = $board->getValidMoves();
        $this->assertCount(2,$moves);
    }

    public function testMove() :void {
        $board = new Board();
        $board->move([1,6],[2,5]);

        $boardField = $board->getBoardField();
        $this->assertEquals($boardField[6][1],"_");
        $this->assertEquals($boardField[5][2],"o");
    }

    public function testOutOfBoundsMove() :void {
        $this->expectExceptionMessage("Invalid move (OOB)");
        $board = new Board();
        $board->move([2,6],[11,5]);
    }

    public function testInvalidCoordinatesMove() :void {
        $this->expectExceptionMessage("Invalid move (coord)");
        $board = new Board();
        $board->move([2],[3,5]);
    }

    public function testNotYourTokenMove() :void {
        $this->expectExceptionMessage("Invalid move (owner)");
        $board = new Board(whitePlayerTurn: false);
        $board->move([2,6],[3,5]);
    }

    public function testInvalidTargetMove() :void {
        $this->expectExceptionMessage("Invalid move (target)");
        $board = new Board(boardField:[
           //0123456789
            " x x x x x",
            "x x x x x ",
            " x x x x x",
            "x x _ _ _ ",
            " _ x x x _",
            "_ o o o _ ",
            " _ _ _ o o",
            "o o o o o ",
            " o o o o o",
            "o o o o o ",
        ]);
        $board->move([2,5],[3,4]);
    }

    public function testInvalidPathMove() :void {
        $this->expectExceptionMessage("Invalid move (path)");
        $board = new Board(boardField:[
            " x x x x x",
            "x x x x x ",
            " x x x x x",
            "x x _ _ _ ",
            " _ x x x _",
            "_ o o o _ ",
            " _ _ _ o o",
            "o o o o o ",
            " o o o o o",
            "o o o o o ",
        ]);
        $board->move([2,5],[9,4]);
    }

    public function testCaptureMove() :void {
        $board = new Board(boardField:[
           //0123456789
            " x x x x x", // 0
            "x x x x x ", // 1
            " x x x x x", // 2
            "x x _ _ _ ", // 3
            " _ x x x _", // 4
            "_ o o o _ ", // 5
            " _ _ _ o o", // 6
            "o o o o o ", // 7
            " o o o o o", // 8
            "o o o o o ", // 9
        ]);
        $board->move([4,5],[6,3]);

        $boardField = $board->getBoardField();

        $this->assertEquals($boardField[3][6],"o");
        $this->assertEquals($boardField[4][5],"!");
        $this->assertEquals($boardField[5][4],"_");
    }

    public function testForceCapture() : void {
        $board = new Board(boardField:[
            //0123456789
            " x x x _ x", // 0
            "x x _ x x ", // 1
            " x x x x x", // 2
            "x x x _ x ", // 3
            " _ x _ _ _", // 4
            "_ o o _ o ", // 5
            " o _ _ _ o", // 6
            "o o o _ o ", // 7
            " o o o o o", // 8
            "o o o o o ", // 9
        ], whitePlayerTurn: false);
        $validMoves = $board->getValidMoves();
        $this->assertCount(1,$validMoves);
    }

    public function testMidMultiJump() : void {
        $board = new Board(boardField:[
           //0123456789
            " _ _ _ _ _", // 0
            "_ _ _ ! _ ", // 1
            " _ _ o _ _", // 2
            "_ _ _ x _ ", // 3
            " x _ _ _ _", // 4
            "_ o _ _ _ ", // 5
            " _ _ _ _ _", // 6
            "_ _ _ _ _ ", // 7
            " _ _ _ _ _", // 8
            "_ _ _ _ _ ", // 9
        ], whitePlayerTurn: true, lastMove: [5,2]);
        $validMoves = $board->getValidMoves();
        $this->assertCount(1,$validMoves);
    }

    public function testKings() : void {
        $board = new Board(boardField:[
           //0123456789
            " O _ _ _ _", // 0
            "_ _ _ _ _ ", // 1
            " _ _ _ _ _", // 2
            "_ _ _ _ _ ", // 3
            " _ _ _ _ _", // 4
            "_ _ _ _ _ ", // 5
            " _ _ _ _ _", // 6
            "_ _ _ _ _ ", // 7
            " _ x _ _ _", // 8
            "_ _ _ _ _ ", // 9
        ]);
        $validMoves = $board->getValidMoves();
        $this->assertCount(9,$validMoves);
    }

    public function testKingsCapture() : void {
        $board = new Board(boardField:[
           //0123456789
            " O _ _ _ _", // 0
            "_ _ _ _ _ ", // 1
            " _ _ _ _ _", // 2
            "_ _ x _ _ ", // 3
            " _ _ _ _ _", // 4
            "_ _ _ _ _ ", // 5
            " _ _ _ _ _", // 6
            "_ _ _ _ _ ", // 7
            " _ x _ _ _", // 8
            "_ _ _ _ _ ", // 9
        ]);
        $validMoves = $board->getValidMoves();
        $this->assertCount(1,$validMoves);
    }

    public function testUpgradeToKing() : void {
        $board = new Board(boardField:[
           //0123456789
            " _ _ _ _ _", // 0
            "_ o _ _ _ ", // 1
            " _ _ _ _ _", // 2
            "_ _ _ _ _ ", // 3
            " _ _ _ _ _", // 4
            "_ _ _ _ _ ", // 5
            " _ _ _ _ _", // 6
            "_ _ _ _ _ ", // 7
            " _ x _ _ _", // 8
            "_ _ _ _ _ ", // 9
        ]);
        $board->move([2,1],[1,0]);
        $boardField = $board->getBoardField();
        $this->assertEquals("O",$boardField[0][1]);
    }

    public function testNotRevertKingAfterCapture() : void {
        $board = new Board(boardField:[
           //0123456789
            " _ _ _ _ _", // 0
            "_ _ _ _ _ ", // 1
            " _ _ _ _ _", // 2
            "_ _ x _ _ ", // 3
            " _ _ O _ _", // 4
            "_ _ _ _ _ ", // 5
            " _ _ _ _ _", // 6
            "_ _ _ _ _ ", // 7
            " _ x _ _ _", // 8
            "_ _ _ _ _ ", // 9
        ]);
        $board->move([5,4],[3,2]);
        $boardField = $board->getBoardField();
        $this->assertEquals("O",$boardField[2][3]);
    }
}
