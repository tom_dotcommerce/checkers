import './App.css';
import {useEffect, useState} from "react";

function generateBoard({size,boardField, whitePlayerTurn, lastMove, validMoves, lastStart},selectedFrom,selectFrom, selectTo, showHint) {
    let result = [];
    for(let y = 0; y < size; y++) {
        for(let x = 0; x < size; x++) {
            let allowFrom = false;
            let classes = ["square"];
            if(boardField[y][x] === " ") {
                classes.push("white-square");
            } else {
                classes.push("black-square");
            }

            validMoves.forEach((move) => {
                if(
                    showHint && (
                        (selectedFrom === null && x === move[0][0] && y === move[0][1]) ||
                        (
                            selectedFrom !== null && selectedFrom[0] === move[0][0] &&
                            selectedFrom[1] === move[0][1] && x === move[1][0] && y === move[1][1]
                        )
                    )
                ) {
                    classes.push("valid-move");
                }
            });

            if(boardField[y][x] === "x" || boardField[y][x] === "X") {
                classes.push("piece");
                classes.push("black-piece");
                allowFrom = !whitePlayerTurn
                if(boardField[y][x] === "X") {
                    classes.push("king");
                }
            }

            if(boardField[y][x] === "o" || boardField[y][x] === "O") {
                classes.push("piece");
                classes.push("white-piece");
                allowFrom = whitePlayerTurn;
                if(boardField[y][x] === "O") {
                    classes.push("king");
                }
            }

            if(boardField[y][x] === "!") {
                classes.push("piece");
                classes.push("captured-piece");
                if(whitePlayerTurn) {
                    classes.push("black-piece");
                } else {
                    classes.push("white-piece");
                }
            }

            if(selectedFrom !== null && x === selectedFrom[0] && y === selectedFrom[1]) {
                classes.push("selected");
            }

            if(lastMove !== null && x === lastMove[0] && y === lastMove[1]) {
                classes.push("last-move");
            }

            if(lastStart !== null && x === lastStart[0] && y === lastStart[1]) {
                classes.push("last-move");
            }

            result.push(<div
                key={`field_${x}_${y}`}
                className={classes.join(" ")}
                onClick={() => {
                    if(boardField[y][x] === "_" && selectedFrom !== null) {
                        selectTo([x, y]);
                    } else {
                        selectFrom(allowFrom ? [x, y] : null);
                        selectTo(null);
                    }
                }}
            />);
        }
    }
    return result;
}

function App() {
    const [loading, setLoading] = useState(true)
    const [game, setGame] = useState({})
    const [selectedFrom, selectFrom] = useState(null);
    const [selectedTo, selectTo] = useState(null);
    const [showHint, setShowHint] = useState(false);
    useEffect(() => {
        const load = async () => {
            let result = await fetch('https://localhost:8000/api/getNewGame');
            let data = await result.json();
            setGame(data);
            setLoading(false);
        };
        load();
    }, []);

    useEffect(() => {
        const move = async (from, to) => {
            const data = new URLSearchParams();
            data.append("data",JSON.stringify(game));
            data.append("move",JSON.stringify([from,to]));
            let result = await fetch("https://localhost:8000/api/makeMove",{
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: data
            });

            let resultData = await result.json();
            if(resultData.hasOwnProperty('error')) {
                alert(resultData.error);
            } else {
                selectTo(null);
                let lastMove = resultData.lastMove;
                if(
                    lastMove !== null &&
                    resultData.whitePlayerTurn &&
                    (
                        resultData.boardField[lastMove[1]][lastMove[0]] === "o" ||
                        resultData.boardField[lastMove[1]][lastMove[0]] === "O"
                    )

                ) {
                    selectFrom(lastMove);
                } else {
                    selectFrom(null);
                }
                setGame(resultData);
            }

        }
        if(selectedFrom !== null && selectedTo !== null) {
            move(selectedFrom,selectedTo);
        }

        if(!game.whitePlayerTurn) {
            setTimeout(() => {
                move([],[]);
            },500);
        }

    }, [selectedFrom, selectedTo, game])

    if (loading) return <div>Loading</div>;

    return (
        <>
        <div className="app">
            {generateBoard(game,selectedFrom,selectFrom,selectTo,showHint)}
            <div><label><input type={"checkbox"} value={showHint} onClick={() => {setShowHint(!showHint)}} /> Show hints</label></div>
        </div>
        </>
    );
}

export default App;
